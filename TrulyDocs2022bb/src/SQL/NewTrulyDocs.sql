CREATE DATABASE  IF NOT EXISTS `NewTrulyDocs` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `NewTrulyDocs`;
-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: NewTrulyDocs
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `author` varchar(45) CHARACTER SET utf8 NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `book_id_UNIQUE` (`book_id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (2,'Teste','Sou Eu é',200),(3,'Teste 2','Também Eu é',300);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document` (
  `document_id` bigint(1) NOT NULL AUTO_INCREMENT,
  `document_st` enum('open','closed','locked','deleted') NOT NULL DEFAULT 'open',
  `document_tp` char(3) NOT NULL,
  `document_cd` tinytext,
  `title_ds` tinytext NOT NULL,
  `short_ds` mediumtext NOT NULL,
  `long_ds` mediumtext,
  `owner_id` bigint(1) DEFAULT NULL,
  `created_dt` datetime NOT NULL,
  `created_user_id` bigint(1) NOT NULL,
  `update_dt` datetime NOT NULL,
  `update_user_id` bigint(1) NOT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (1,'open','xxx','teste','Teste','Isto é um teste','Isto é um teste muito comprido',1,'2021-05-06 12:00:00',1,'2021-05-06 12:00:00',1);
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `file` (
  `file_id` bigint(1) NOT NULL,
  `document_id` bigint(1) NOT NULL,
  `file_st` enum('available','restricted','locked','deleted') NOT NULL,
  `uuid_fn` tinytext NOT NULL,
  `content_tp` char(3) DEFAULT NULL,
  `file_ds` mediumtext,
  `first_upload_dt` datetime NOT NULL,
  `first_upload_user_id` bigint(1) NOT NULL,
  `last_upload_dt` datetime NOT NULL,
  `last_upload_user_id` bigint(1) NOT NULL,
  `original_fn` tinytext,
  `original_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `fk_file_document_idx` (`document_id`),
  KEY `fk_file_file_type_idx` (`content_tp`),
  CONSTRAINT `fk_file_document` FOREIGN KEY (`document_id`) REFERENCES `document` (`document_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_file_file_type` FOREIGN KEY (`content_tp`) REFERENCES `file_type` (`type_cd`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_extension`
--

DROP TABLE IF EXISTS `file_extension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `file_extension` (
  `extension_cd` char(3) NOT NULL,
  `type_cd` char(3) NOT NULL,
  `extension_fn` tinytext NOT NULL,
  PRIMARY KEY (`extension_cd`),
  KEY `fk_file_type_idx` (`type_cd`),
  CONSTRAINT `fk_file_type` FOREIGN KEY (`type_cd`) REFERENCES `file_type` (`type_cd`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_extension`
--

LOCK TABLES `file_extension` WRITE;
/*!40000 ALTER TABLE `file_extension` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_extension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_type`
--

DROP TABLE IF EXISTS `file_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `file_type` (
  `type_cd` char(3) NOT NULL,
  `type_ds` tinytext NOT NULL,
  `acceptable_fl` tinyint(4) NOT NULL,
  `previewable_fl` tinyint(4) NOT NULL,
  `file_cmd_regex` mediumtext NOT NULL,
  `changed_dt` datetime NOT NULL,
  PRIMARY KEY (`type_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_type`
--

LOCK TABLES `file_type` WRITE;
/*!40000 ALTER TABLE `file_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `version`
--

DROP TABLE IF EXISTS `version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `version` (
  `version_id` bigint(1) NOT NULL,
  `file_id` bigint(1) NOT NULL,
  `uuid_fn` tinytext NOT NULL,
  `content_tp` char(3) DEFAULT NULL,
  `upload_dt` datetime NOT NULL,
  `upload_user_id` bigint(1) NOT NULL,
  `original_fn` tinytext,
  `original_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`version_id`),
  KEY `fk_version_file_idx` (`file_id`),
  KEY `fk_version_file_type_idx` (`content_tp`),
  CONSTRAINT `fk_version_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_version_file_type` FOREIGN KEY (`content_tp`) REFERENCES `file_type` (`type_cd`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `version`
--

LOCK TABLES `version` WRITE;
/*!40000 ALTER TABLE `version` DISABLE KEYS */;
/*!40000 ALTER TABLE `version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'NewTrulyDocs'
--

--
-- Dumping routines for database 'NewTrulyDocs'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-06 13:35:52
