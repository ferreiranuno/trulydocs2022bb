package pt.agilefactor.trulydocs;

public class Files {
	protected String file_id;
	protected String document_id;
	protected String first_upload_user_id;
	protected String last_upload_user_id;
	protected String file_ds;
	protected String first_upload_dt;
	protected String last_upload_dt;
	protected String content_tp;
	protected String uuid_fn;
	protected String file_nm;

	public Files() {
	}

	public Files(String file_id) {
		this.file_id = file_id;
	}

	public Files(String file_id,
			String document_id,
			String first_upload_user_id,
			String last_upload_user_id,
			String file_ds,
			String first_upload_dt,
			String last_upload_dt,
			String content_tp,
			String uuid_fn,
			String file_nm) {
		this(document_id, first_upload_user_id, last_upload_user_id, file_ds, first_upload_dt, last_upload_dt,
				content_tp, uuid_fn, file_nm);
		this.file_id = file_id;
	}

	public Files(String document_id,
			String first_upload_user_id,
			String last_upload_user_id,
			String file_ds,
			String first_upload_dt,
			String last_upload_dt,
			String content_tp,
			String uuid_fn,
			String file_nm) {
		this.document_id = document_id;
		this.first_upload_user_id = first_upload_user_id;
		this.last_upload_user_id = last_upload_user_id;
		this.file_ds = file_ds;
		this.first_upload_dt = first_upload_dt;
		this.last_upload_dt = last_upload_dt;
		this.content_tp = content_tp;
		this.uuid_fn = uuid_fn;
		this.file_nm = file_nm;
	}

	public String getFile_id() {
		return this.file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

	public String getDocument_id() {
		return this.document_id;
	}

	public void setDocument_id(String document_id) {
		this.document_id = document_id;
	}

	public String getFirst_upload_user_id() {
		return this.first_upload_user_id;
	}

	public void setFirst_upload_user_id(String first_upload_user_id) {
		this.first_upload_user_id = first_upload_user_id;
	}

	public String getLast_upload_user_id() {
		return this.last_upload_user_id;
	}

	public void setLast_upload_user_id(String last_upload_user_id) {
		this.last_upload_user_id = last_upload_user_id;
	}

	public String getFile_ds() {
		return this.file_ds;
	}

	public void setFile_ds(String file_ds) {
		this.file_ds = file_ds;
	}

	public String getFirst_upload_dt() {
		return this.first_upload_dt;
	}

	public void setFirst_upload_dt(String first_upload_dt) {
		this.first_upload_dt = first_upload_dt;
	}

	public String getLast_upload_dt() {
		return this.last_upload_dt;
	}

	public void setLast_upload_dt(String last_upload_dt) {
		this.last_upload_dt = last_upload_dt;
	}

	public String getContent_tp() {
		return this.content_tp;
	}

	public void setContent_tp(String content_tp) {
		this.content_tp = content_tp;
	}

	public String getUuid_fn() {
		return this.uuid_fn;
	}

	public void setUuid_fn(String uuid_fn) {
		this.uuid_fn = uuid_fn;
	}

	public String getFile_nm() {
		return this.file_nm;
	}

	public void setFile_nm(String file_nm) {
		this.file_nm = file_nm;
	}
}