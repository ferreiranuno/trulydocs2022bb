package pt.agilefactor.trulydocs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class FilesDAO {
	private String jdbcURL;
	private String jdbcUsername;
	private String jdbcPassword;
	private Connection jdbcConnection;

	public FilesDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) {
		this.jdbcURL = jdbcURL;
		this.jdbcUsername = jdbcUsername;
		this.jdbcPassword = jdbcPassword;
	}

	protected void connect() throws SQLException {
		if (this.jdbcConnection == null || this.jdbcConnection.isClosed()) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver"); //$NON-NLS-1$
			} catch (ClassNotFoundException e) {
				throw new SQLException(e);
			}
			this.jdbcConnection = DriverManager.getConnection(this.jdbcURL, this.jdbcUsername, this.jdbcPassword);
		}
	}

	protected void disconnect() throws SQLException {
		if (this.jdbcConnection != null && !this.jdbcConnection.isClosed()) {
			this.jdbcConnection.close();
		}
	}

	public boolean insertFilesInDoc(Files files) throws SQLException {
		String sql = "INSERT INTO file (document_id, file_nm, file_ds, uuid_fn, first_upload_dt, last_upload_dt, first_upload_user_id, last_upload_user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"; //$NON-NLS-1$
		boolean rowInserted = false;
		connect();
		try (PreparedStatement statement = this.jdbcConnection.prepareStatement(sql);) {
//			PreparedStatement statement = this.jdbcConnection.prepareStatement(sql);
			statement.setString(1, files.getDocument_id());
			statement.setString(2, files.getFile_nm());
			statement.setString(3, files.getFile_ds());
			statement.setString(4, files.getUuid_fn());
			statement.setString(5, files.getFirst_upload_dt());
			statement.setString(6, files.getLast_upload_dt());
			statement.setString(7, files.getFirst_upload_user_id());
			statement.setString(8, files.getLast_upload_user_id());

			rowInserted = statement.executeUpdate() > 0;
			statement.close();
		}
		disconnect();
		return rowInserted;
	}
	
	
	public boolean updatesFiles(Files files) throws SQLException {
		String sql = "UPDATE file SET file_nm = ?, file_ds = ?, uuid_fn = ?, last_upload_dt = ? WHERE file_id = ?";
		boolean rowUpdated = false;
		connect();
		try (PreparedStatement statement = this.jdbcConnection.prepareStatement(sql)) {
			statement.setString(1, files.getFile_nm());
			statement.setString(2, files.getFile_ds());
			statement.setString(3, files.getUuid_fn());
			statement.setString(4, files.getLast_upload_dt());
			statement.setString(5, files.getFile_id());
			
			rowUpdated = statement.executeUpdate() > 0; 
			statement.close();
		}
		disconnect();
		return rowUpdated;	
	}
	
	
	
	public boolean deletefiles(int file_id) throws SQLException {
		String sql = "DELETE FROM file WHERE file_id = ?"; //$NON-NLS-1$
		boolean rowDeleted = false;
		connect();
		try (PreparedStatement statement = this.jdbcConnection.prepareStatement(sql);) {
			statement.setInt(1, file_id);
			rowDeleted = statement.executeUpdate() > 0;
			statement.close();
		}
		disconnect();
		return rowDeleted;
	}

	public static List<Files> listAllFiles() {
		// TODO Auto-generated method stub
		return null;
	}

}
