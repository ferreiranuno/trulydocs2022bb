package pt.agilefactor.trulydocs;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns= {"/iniP", "/lstD", "/lstF", "/addD", "/addF", "/delD", "/delF", "/lstB", "/updD", "/updF"})
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DocsDAO docsDAO;
	private static FilesDAO filesDAO;

	@Override
	public void init() {
		String jdbcURL = getServletContext().getInitParameter("jdbcURL"); //$NON-NLS-1$
		String jdbcUsername = getServletContext().getInitParameter("jdbcUsername"); //$NON-NLS-1$
		String jdbcPassword = getServletContext().getInitParameter("jdbcPassword"); //$NON-NLS-1$

		this.docsDAO = new DocsDAO(jdbcURL, jdbcUsername, jdbcPassword);
		this.filesDAO = new FilesDAO(jdbcURL, jdbcUsername, jdbcPassword);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getServletPath();

		try {
			switch (action) {
//			case "/bs_new": //$NON-NLS-1$
//				// showNewForm(request, response);
//				break;
			case "/addD": //$NON-NLS-1$
				insertDocs(request, response);
				break;
			case "/addF": //$NON-NLS-1$
				insertFilesInDoc(request, response);
				break;
			case "/delD": //$NON-NLS-1$
				deletedocs(request, response);
				break;
			case "/delF":
				deletefiles(request, response);
				break;
			case "/lstB": //$NON-NLS-1$
				list_files_doc(request, response);
				break;
			case "/updD": //$NON-NLS-1$
				updateDocs(request, response);
				break;
			case "/updF": //$NON-NLS-1$
				updateFiles(request, response);
				break;
			case "/lstD": //$NON-NLS-1$
				listDocs(request, response);
				break;
			case "/lstF": //$NON-NLS-1$
				listFichs(request, response);
				break;
			case "/iniP": //$NON-NLS-1$
				inicial(request, response);
				break;
			default:
				inicial(request, response);
				break;
			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}
		/*int page = 1;
        int recordsPerPage = 5;
        if(request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
        DocsDAO dao = new DocsDAO();
        List<Docs> list = dao.listAllDocs((page-1)*recordsPerPage,recordsPerPage);
        int noOfRecords = dao.getNoOfRecords();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("DocsList", list);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);
        RequestDispatcher view = request.getRequestDispatcher("lstD.jsp");
        view.forward(request, response);*/
	}

	private static void inicial(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("Inicial.jsp"); //$NON-NLS-1$
		dispatcher.forward(request, response);
	}

	private void listDocs(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		List<Docs> listDocs = this.docsDAO.listAllDocs();
		request.setAttribute("listDocs", listDocs); //$NON-NLS-1$
		RequestDispatcher dispatcher = request.getRequestDispatcher("List_Docs.jsp"); //$NON-NLS-1$
		dispatcher.forward(request, response);
	}

	private static void listFichs(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<Files> listFiles = FilesDAO.listAllFiles();
		request.setAttribute("listFiles", listFiles); //$NON-NLS-1$
		RequestDispatcher dispatcher = request.getRequestDispatcher("List_Ficheiros.jsp"); //$NON-NLS-1$
		dispatcher.forward(request, response);
	}

	/*
	 * private void showNewForm(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { RequestDispatcher dispatcher
	 * = request.getRequestDispatcher("bookstore/BookForm.jsp");
	 * dispatcher.forward(request, response); }
	 */

	private void list_files_doc(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, ServletException, IOException {
		String document_id = request.getParameter("document_id"); //$NON-NLS-1$
		List<Files> listFiles = this.docsDAO.listAllFiles(document_id);
		request.setAttribute("listFiles", listFiles); //$NON-NLS-1$

		List<Docs> listDoc = this.docsDAO.listDoc(document_id);
		request.setAttribute("listDoc", listDoc); //$NON-NLS-1$
		request.setAttribute("document_id", document_id); //$NON-NLS-1$

		RequestDispatcher dispatcher = request.getRequestDispatcher("List_Fichs_Doc.jsp"); //$NON-NLS-1$
		dispatcher.forward(request, response);
	}

	private void insertDocs(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$
		LocalDateTime now = LocalDateTime.now();
		String title_ds = request.getParameter("title_ds"); //$NON-NLS-1$
		String document_tp = "xxx"; //$NON-NLS-1$
		String created_dt = dtf.format(now);
		String update_dt = dtf.format(now);
		String short_ds = request.getParameter("short_ds"); //$NON-NLS-1$
		String created_user_id = "1"; //$NON-NLS-1$
		String update_user_id = "1"; //$NON-NLS-1$
		String long_ds = request.getParameter("long_ds"); //$NON-NLS-1$

		Docs newDocs = new Docs(title_ds, document_tp, created_dt, update_dt, short_ds, created_user_id, update_user_id,
				long_ds);
		this.docsDAO.insertDocs(newDocs);
		response.sendRedirect("lstD"); //$NON-NLS-1$
	}
	private void updateDocs (HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String document_id = request.getParameter("document_id");
		String title_ds = request.getParameter("title_ds");
		String document_tp = "xxx";
		String created_dt = request.getParameter("created_dt");
		String update_dt = dtf.format(now); 
		String short_ds = request.getParameter("short_ds");
		String created_user_id = "1";
		String update_user_id = "1";
		String long_ds = request.getParameter("long_ds");
		
		Docs upDocs = new Docs(document_id, title_ds, document_tp, created_dt, update_dt, short_ds, created_user_id, update_user_id,
					long_ds);
		this.docsDAO.updatesDocs(upDocs);
		response.sendRedirect("lstD");
	}
	
	/*
	private void updateDocs(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException { 
		int document_id = Integer.parseInt(request.getParameter("document_id"));
		String title_ds = request.getParameter("title_ds");
		String created_dt = request.getParameter("created_dt");
		String update_dt = request.getParameter("update_dt");
		String short_ds = request.getParameter("short_ds");
	  
		Docs docs = new Docs(document_id, title_ds, created_dt, update_dt, short_ds);
		this.docsDAO.updateDocs(docs);
		response.sendRedirect("lstD"); 
	  }
	*/
	private void insertFilesInDoc(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$
		LocalDateTime now = LocalDateTime.now();
		String file_nm = request.getParameter("file_nm"); //$NON-NLS-1$
		String file_ds = request.getParameter("file_ds"); //$NON-NLS-1$
		String uuid_fn = request.getParameter("uuid_fn"); //$NON-NLS-1$
		String first_upload_dt = dtf.format(now);
		String last_upload_dt = dtf.format(now);
		String first_upload_user_id = "1"; //$NON-NLS-1$
		String last_upload_user_id = "1"; //$NON-NLS-1$
		String content_tp = "PDF"; //$NON-NLS-1$
		String document_id = request.getParameter("doc_id"); //$NON-NLS-1$

		Files newFiles = new Files(document_id, first_upload_user_id, last_upload_user_id, file_ds,
				first_upload_dt, last_upload_dt, content_tp, uuid_fn, file_nm);
		this.filesDAO.insertFilesInDoc(newFiles);
		response.sendRedirect("lstD"); //$NON-NLS-1$
	}
	
	private void updateFiles (HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String files_id = request.getParameter("files_id");
		String file_nm = request.getParameter("file_nm"); //$NON-NLS-1$
		String file_ds = request.getParameter("file_ds"); //$NON-NLS-1$
		String uuid_fn = request.getParameter("uuid_fn"); //$NON-NLS-1$
		String first_upload_dt = request.getParameter("first_upload_dt");
		String last_upload_dt = dtf.format(now);
		String first_upload_user_id = "1"; //$NON-NLS-1$
		String last_upload_user_id = "1"; //$NON-NLS-1$
		String content_tp = "PDF"; //$NON-NLS-1$
		
		
		Files upFiles = new Files(files_id, first_upload_user_id, last_upload_user_id, file_ds,
				first_upload_dt, last_upload_dt, content_tp, uuid_fn, file_nm);
		this.filesDAO.updatesFiles(upFiles);
		response.sendRedirect("lstD");
	}
	  	 
	private static void deletedocs(HttpServletRequest request, HttpServletResponse response) throws IOException, NumberFormatException, SQLException {

		String ids[] = request.getParameter("ids").split(","); //$NON-NLS-1$ //$NON-NLS-2$
		
			for (String id: ids) {;
		  		docsDAO.delete(Integer.parseInt(id));
		  	}
		response.sendRedirect("lstD"); //$NON-NLS-1$
	}
	
	private static void deletefiles(HttpServletRequest request, HttpServletResponse response) throws IOException, NumberFormatException, SQLException {

		String ids[] = request.getParameter("ids").split(","); 
		
			for (String id: ids) {;
		  		filesDAO.deletefiles(Integer.parseInt(id));
		  	}
		response.sendRedirect("lstD"); 
	}
}