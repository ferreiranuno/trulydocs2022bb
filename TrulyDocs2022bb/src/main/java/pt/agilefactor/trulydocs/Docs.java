package pt.agilefactor.trulydocs;

public class Docs {
	protected String document_id;
	protected String title_ds;
	protected String document_tp;
	protected String created_dt;
	protected String update_dt;
	protected String short_ds;
	protected String created_user_id;
	protected String update_user_id;
	protected String long_ds;

	public Docs() {
	}

	public Docs(String document_id) {
		this.document_id = document_id;
	}

	/*public Docs(String document_id, String title_ds, String document_tp, String created_dt, String update_dt,
			String short_ds, String created_user_id, String update_user_id, String long_ds) {
		this(title_ds, document_tp, created_dt, update_dt, short_ds, created_user_id, update_user_id, long_ds);
		this.document_id = document_id;
	}*/

	public Docs(String title_ds, String document_tp, String created_dt, String update_dt, String short_ds,
			String created_user_id, String update_user_id, String long_ds) {
		this.title_ds = title_ds;
		this.document_tp = document_tp;
		this.created_dt = created_dt;
		this.update_dt = update_dt;
		this.short_ds = short_ds;
		this.created_user_id = created_user_id;
		this.update_user_id = update_user_id;
		this.long_ds = long_ds;
	}
	
	public Docs(String document_id, String title_ds, String document_tp, String created_dt, String update_dt, String short_ds,
			String created_user_id, String update_user_id, String long_ds) {
		this.document_id = document_id;
		this.title_ds = title_ds;
		this.document_tp = document_tp;
		this.created_dt = created_dt;
		this.update_dt = update_dt;
		this.short_ds = short_ds;
		this.created_user_id = created_user_id;
		this.update_user_id = update_user_id;
		this.long_ds = long_ds;
	}

	public String getDocument_id() {
		return this.document_id;
	}

	public void setDocument_id(String document_id) {
		this.document_id = document_id;
	}

	public String getTitle_ds() {
		return this.title_ds;
	}

	public void setTitle_ds(String title_ds) {
		this.title_ds = title_ds;
	}

	public String getDocument_tp() {
		return this.document_tp;
	}

	public void setDocument_tp(String document_tp) {
		this.document_tp = document_tp;
	}

	public String getCreated_dt() {
		return this.created_dt;
	}

	public void setCreated_dt(String created_dt) {
		this.created_dt = created_dt;
	}

	public String getUpdate_dt() {
		return this.update_dt;
	}

	public void setUpdate_dt(String update_dt) {
		this.update_dt = update_dt;
	}

	public String getShort_ds() {
		return this.short_ds;
	}

	public void setShort_ds(String short_ds) {
		this.short_ds = short_ds;
	}

	public String getCreated_user_id() {
		return this.created_user_id;
	}

	public void setCreated_user_id(String created_user_id) {
		this.created_user_id = created_user_id;
	}

	public String getUpdate_user_id() {
		return this.update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public String getLong_ds() {
		return this.long_ds;
	}

	public void setLong_ds(String long_ds) {
		this.long_ds = long_ds;
	}

}