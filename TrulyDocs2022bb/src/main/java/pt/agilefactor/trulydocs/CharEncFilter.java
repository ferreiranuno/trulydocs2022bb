package pt.agilefactor.trulydocs;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter("/*")
public class CharEncFilter implements Filter {

	@Override
	public void init(FilterConfig config) throws ServletException {
		// NOOP.
	}

	@Override
	public void destroy() {
		// NOOP.
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8"); //$NON-NLS-1$
		chain.doFilter(request, response);
	}
}