<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>TrulyDocs</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	</head>
<body>
	<div class="body">
		<img id="photo1" src="img/agile.png" alt="AgileFactor" class="center">
		<div class="nav">
			<ul>
				<li><a href="iniP">Página inicial</a></li>
				<li><a href="lstD">Documentos</a></li>
			</ul>
		</div>
		<div>
			<div class="container_principal" style="width:70%;">
				<div class="nav navbuttons">
					<ul>
						<li><button class="button">Download Selected</button></li>
						<li><button class="button" onclick="deleteSelected('doc')">Delete Selected</button></li>
					</ul>
				</div>
				<div>
					<table class="table">
						<thead>
							<tr>
								<th><input type="checkbox" id="checkall" onclick="toggle()"></th>
								<th>Name</th>
								<th>Short description</th>
								<th>Date</th>
								<th>Last Updated</th>
								<th>Operations</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="docs" items="${listDocs}">
								<tr>
									<td><input type="checkbox" class="myboxitem" value="<c:out value='${docs.document_id}'/>"></td>
									<td><a href="lstB?document_id=<c:out value='${docs.document_id}'/>"><c:out value='${docs.title_ds}'/></a></td>
									<td><c:out value='${docs.short_ds}'/></td>
									<td><c:out value='${docs.created_dt}'/></td>
									<td><c:out value='${docs.update_dt}'/></td>
									<td><button class="editbutton" id="editFile" onclick="openMod(${docs.document_id})">Edit</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<div class="pag">
						 <%--For displaying Previous link except for the 1st page --%>
	    				<c:if test="${currentPage != 1}">
	        				<td><a href="lstD?page=${currentPage - 1}">Anterior</a></td>
	    				</c:if>
	 
	    				<%--For displaying Page numbers. 
	    				The when condition does not display a link for the current page--%>
	    				<table>
	        				<tr>
		            			<c:forEach begin="1" end="${noOfPages}" var="i">
		                			<c:choose>
		                    			<c:when test="${currentPage eq i}">
		                        			<td>${i}</td>
		                    			</c:when>
		                    			<c:otherwise>
		                        			<td><a href="lstD?page=${i}">${i}</a></td>
		                    			</c:otherwise>
		                			</c:choose>
		            			</c:forEach>
	        				</tr>
	   		 			</table>
					    <%--For displaying Next link --%>
					    <c:if test="${currentPage lt noOfPages}">
					        <td><a href="lstD?page=${currentPage + 1}">Próximo</a></td>
					    </c:if>
					</div>
				</div>
			</div>
			<div class="container_principal" style="width:25%;float:right;">
				<div class="divprocura">		
					<input type="text" id="txtprocura" placeholder="Procurar..." />
					<a class="buttonP" href="javascript:void(0);">
						<img src="img/lupa.png" id="btnprocura" alt="Procurar" width="30" height="30">
					</a>
				</div>
				<div class="createdoc">
					<h2>Create Doc</h2>
					<form action="<%=request.getContextPath()%>/addD" method="post">
						<label><input type="text" pattern=".{1,}" required title="Têm de ter pelo menos 1 letra!" name="title_ds" placeholder="title...."/></label>
						<label><input type="text" name="short_ds" placeholder="short description..."/></label>
						<label><input type="text" name="long_ds" placeholder="long description..."/></label>
						<button type="submit">Submit</button>
					</form>
				</div>
				
				
				
				<div class="uploadfile">
					<!-- Modal -->
					<div id="myModal2" class="back">
						<!-- Conteudo da modal -->
						<div class="conteudo">
							<div class="header"></div>
							<div class="modalbody">
								<span class="close2">&times;</span>
								<form id="formEdit" class="formAlign" action="<%=request.getContextPath()%>/updD" method="post">
									<h3>Edit do Documento:</h3>
									<% String val = request.getParameter("document_id"); %>
									<input type="text" name="title_ds" placeholder="document title..." /> 
									<input type="text" name="short_ds" placeholder="document short description..." /> 
									<input type="text" name="long_ds" placeholder="document long description..." />
									<input type="hidden" id="modal_editdocid" name="document_id" value="" />  
									<input type="submit" value="Upload">
								</form>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>