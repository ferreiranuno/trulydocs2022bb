<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>TrulyDocs</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
</head>
<body>
	<div class="body">
		<img id="photo1" src="img/agile.png" alt="AgileFactor" class="center">
		<div class="nav">
			<ul>
				<li><a href="iniP">Página inicial</a></li>
				<li><a href="lstD">Documentos</a></li>
			</ul>
		</div>
		<div>
			<div class="container_principal" style="width: 70%;">
				<div class="nav navbuttons">
					<ul>
						<li><button class="button">Download Selected</button></li>
						<li><button class="button" onclick="deleteSelected('file')">Delete Selected</button></li>
						<li><button class="button" id="createFile">Adicionar ficheiro</button></li>
					</ul>
				</div>
				<div>
					<table class="table">
						<c:if test="${docs != null}">
							<input type="hidden" name="id" value="<c:out value='${docs.document_id}' />" />
						</c:if>
						<thead>
							<tr>
								<th><input type="checkbox" id="checkall" onclick="toggle()"></th>
								<th>Name</th>
								<th>Short description</th>
								<th>Date</th>
								<th>Last Updated</th>
								<th>Operations</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="files" items="${listFiles}">
								<tr>
									<td><input type="checkbox" class="myboxitem" value="<c:out value='${files.file_id}'/>"></td>
									<td><c:out value='${files.file_nm}' /></td>
									<td><c:out value='${files.file_ds}' /></td>
									<td><c:out value='${files.first_upload_dt}' /></td>
									<td><c:out value='${files.last_upload_dt}' /></td>
									<td><button class="editbutton" id="editFile" onclick="openMod2(${files.file_id})">Edit</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

			</div>
			<div class="container_principal" style="width: 25%; float: right;">
				<div class="divprocura">
					<input type="text" id="txtprocura" placeholder="Procurar..." /> <a
						class="buttonP" href="javascript:void(0);"> <img
						src="img/lupa.png" id="btnprocura" alt="Procurar" width="30"
						height="30">
					</a>
				</div>
				<div class="side">
					<div class="inside" style="width: 20%;">
						<img src="img/doc.png" alt="doc">
					</div>
					<div class="inside" style="width: 70%;">
						<c:forEach var="doc" items="${listDoc}">
							<h3>
								Nome:
								<c:out value='${doc.title_ds}' />
							</h3>
							<h3>
								Descrição:
								<c:out value='${doc.long_ds}' />
							</h3>
							<h3>
								Criado em
								<c:out value='${doc.created_dt}' />
							</h3>
							<h3>
								Atualizado em
								<c:out value='${doc.update_dt}' />
							</h3>
						</c:forEach>
					</div>
				</div>



				<div class="uploadfile">
					<!-- Modal -->
					<div id="myModal" class="back">
						<!-- Conteudo da modal -->
						<div class="conteudo">
							<div class="header"></div>
							<div class="modalbody">
								<span class="close">&times;</span>
								<form class="formAlign" action="<%=request.getContextPath()%>/addF" method="post">
									<h3>Ficheiro:</h3>
									<% String val = request.getParameter("document_id"); %>
									<input type="file" id="fileInput" name="file_nm" size="200" />
									<input type="text" name="file_ds" placeholder="file description..." /> 
									<input type="hidden" name="doc_id" value="<c:out value='${document_id}' />" /> 
									<input type="text" name="file_st" placeholder="file state..." /> 
									<input type="text" name="uuid_fn" placeholder="custom uuid..." /> 
									<input type="submit" value="Upload">
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
				<div class="uploadfile">
					<!-- Modal -->
					<div id="myModal3" class="back">
						<!-- Conteudo da modal -->
						<div class="conteudo">
							<div class="header"></div>
							<div class="modalbody">
								<span class="close2">&times;</span>
								<form id="formEdit" class="formAlign" action="<%=request.getContextPath()%>/updF" method="post">
									<h3>Edit do File:</h3>
									<% String vall = request.getParameter("file_id"); %>
									<input type="text" name="file_nm" placeholder="File title..." /> 
									<input type="text" name="file_ds" placeholder="File short description..." /> 
									<input type="text" name="uuid_fn" placeholder="File uuid..." />
									<input type="hidden" id="modal_editfileid2" name="file_id" value="" />  
									<input type="submit" value="Upload">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>