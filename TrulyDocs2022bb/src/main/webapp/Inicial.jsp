<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<title>TrulyDocs</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	</head>
<body>
<div class="body">
	<img id="photo1" src="img/agile.png" alt="AgileFactor" class="center">
	<div class="nav">
		<ul>
			<li><a href="iniP">Página inicial</a></li>
			<li><a href="lstD">Documentos</a></li>
		</ul>
	</div>
</div>
</body>
</html>