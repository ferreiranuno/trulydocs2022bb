<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TrulyDocs</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>
<img id="photo1" src="img/agile.png" alt="AgileFactor" class="img_agile">
<button class="button1" onClick="history.go(-1)"><img id="photo2" src="img/voltar.png" alt="voltar" class="img_voltar"></button>
<div class="sidenav2">
  <a href="inicial">Página inicial</a>
  <a href="list_docs">Documentos</a>
</div>
<div class="createdoc">
Create Doc
<form action="<%=request.getContextPath()%>/bs_insert" method="post">
<input type="text" pattern=".{1,}" required title="Têm de ter pelo menos 1 letra!" name="title_ds" placeholder="title...."/>
<input type="text" name="short_ds" placeholder="short description..."/>
<input type="text" name="long_ds" placeholder="long description..."/>
<input type="submit">
</form>
</div>
<div >
	<button class="button2">Download Selected</button>	
	<button class="button2 button3" onclick="deleteSelected()">Delete Selected</button>
</div>	
<div id="divprocura" class="barra">
  <input type="text" id="txtprocura" placeholder="Procurar..." />
  <button class="buttonP" type="submit"><img src="img/lupa.png" id="btnprocura" alt="Procurar" class="img_lupa"></button>
</div>
<div class="table">
<table>
 <tr>
    <th class="tablecab tablecab1"><input type="checkbox" id="checkall" onclick="toggle()"></th>
    <th class="tablecab tablecab2">Name</th>
    <th class="tablecab tablecab3">Short description</th>
    <th class="tablecab tablecab4">Date</th>
    <th class="tablecab tablecab5">Last Updated</th>
    <th class="tablecab tablecab6">Operations</th>
 </tr>
<c:forEach var="docs" items="${listDocs}">
 <tr>
    <th><input type="checkbox" class="myboxitem" value="<c:out value='${docs.document_id}'/>"></th>
    <th class="nd"><a href="bs_fichs?document_id=<c:out value='${docs.document_id}'/>"><c:out value='${docs.title_ds}'/></a></th>
   	<th class="nd"><c:out value='${docs.short_ds}'/></th>
    <th><c:out value='${docs.created_dt}'/></th>
    <th><c:out value='${docs.update_dt}'/></th>
    <th><c:out value='${docs.document_tp}'/></th>
 </tr>
 </c:forEach>
 </table>
</div>
</body>
</html>