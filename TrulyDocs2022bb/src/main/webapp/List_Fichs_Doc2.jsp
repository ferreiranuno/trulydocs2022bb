<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<%@ page import="javax.servlet.http.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<meta charset="UTF-8">
<title>TrulyDocs</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>
	<img id="photo1" src="img/agile.png" alt="AgileFactor"
		class="img_agile">
	<button class="button1" onClick="history.go(-1)">
		<img id="photo2" src="img/voltar.png" alt="voltar" class="img_voltar">
	</button>
	<img id="photo3" src="img/doc.png" alt="doc" class="img_doc">
	<c:forEach var="doc" items="${listDoc}">
		<div class="side">
			<a>Nome: <c:out value='${doc.title_ds}' /><br></a> <a>Descrição:
				<c:out value='${doc.long_ds}' /><br>
			</a> <a>Criado em <c:out value='${doc.created_dt}' /><br></a> <a>Atualizado
				em <c:out value='${doc.update_dt}' />
			</a>
		</div>
	</c:forEach>
	<div class="uploadfile">
		Upload File <br>
		<!--  <a href="list_fichs">Ficheiros</a>-->
		<button id="createFile">Adicionar ficheiro</button>
		<!-- Modal -->
		<div id="myModal" class="modalBack">
			<!-- Conteudo da modal -->
			<div class="modalConteudo">
				<div class="modal-header">
					<span class="close">&times;</span>
				</div>
				<div class="modal-body">
					<form class="formAlign"
						action="<%=request.getContextPath()%>/bs_insert_files"
						method="post">
						<td>Ficheiro:</td>
						<% String val = request.getParameter("document_id"); %>
						<td><input type="file" id="fileInput" name="file_nm"
							size="500" /></td>
						<br> <input type="text" name="file_ds"
							placeholder="file description..." /> <input type="hidden"
							name="doc_id" value="<c:out value='${document_id}' />" /> <input
							type="text" name="file_st" placeholder="file state..." /> <input
							type="text" name="uuid_fn" placeholder="custom uuid..." /> <input
							type="submit" value="Upload">
					</form>
				</div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<div class="sidenav">
		<a href="inicial">Página inicial</a> <a href="list_docs">Documentos</a>
	</div>
	<div>
		<button class="button2">Download Selected</button>
		<button class="button2 button3">Delete Selected</button>
	</div>
	<div id="divprocura" class="barra">
		<input type="text" id="txtprocura" placeholder="Procurar..." />
		<button class="buttonP" type="submit">
			<img src="img/lupa.png" id="btnprocura" alt="Procurar"
				class="img_lupa">
		</button>
	</div>

	<div class="table">
		<table>
			<c:if test="${docs != null}">
				<input type="hidden" name="id"
					value="<c:out value='${docs.document_id}' />" />
			</c:if>
			<tr>
				<th class="tablecab tablecab7"><input type="checkbox"
					name=mybox onClick="toggle(this)"></th>
				<th class="tablecab tablecab8">Name</th>
				<th class="tablecab tablecab9">Short description</th>
				<th class="tablecab tablecab10">Date</th>
				<th class="tablecab tablecab11">Last Updated</th>
				<th class="tablecab tablecab12">Operations</th>
			</tr>
			<c:forEach var="files" items="${listFiles}">
				<tr>
					<th><input type="checkbox" name=mybox value="1"></th>
					<th class="nd"><c:out value='${files.file_nm}' /></th>
					<th class="nd"><c:out value='${files.file_ds}' /></th>
					<th><c:out value='${files.first_upload_dt}' /></th>
					<th><c:out value='${files.last_upload_dt}' /></th>
					<th></th>
				</tr>
			</c:forEach>
		</table>
	</div>

	<script type="text/javascript" src="js/scriptsTiago.js"></script>
</body>
</html>